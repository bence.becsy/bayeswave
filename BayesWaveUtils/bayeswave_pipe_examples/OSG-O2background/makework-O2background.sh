#!/bin/sh -e


bayeswave_pipe \
        O2background.ini \
        --workdir O2background \
        --cwb-trigger-list 100_cwb_triggers.dat \
        --osg-jobs \
        --glide-in \
        --skip-post \
        --skip-megapy 

